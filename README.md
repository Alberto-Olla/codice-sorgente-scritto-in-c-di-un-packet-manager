# Packet Manager

Questo è il progetto che ho svolto in C per l'esame di Sistemi Operativi nell'anno scolastico 2010/2011. In tutto è costituito da tre programmi che simulano le funzionalità di un gestore di pacchetti (per chi non usa Linux, è un programma che si occupa di trovare e installare automaticamente i programmi di cui avete bisogno).

I tre programmi sono: server, client di upload e client di download. Le applicazioni una volta avviate eseguono delle istruzioni specificate da file (comunque già presenti, non dovete preoccuparvi di crearne delle nuove per testare il tutto). I risultati delle operazioni eseguite dai programmi non possono essere osservate guardando l'output del terminale ma bensì guardando i file di log che verranno prodotti durante l'esecuzione.

## Come si usa

Aprite tre finestre del terminale nella cartella `codice`. Riporterò ora i comandi che dovrete inserire in ognuno dei terminali.

Nel primo terminale: `make servertest`

Nel secondo terminale: `make uptest`

Nel terzo terminale: `make downtest`

Vedrete che il server e i due client si avvieranno e inizieranno a fare il loro lavoro: il server aspetterà che gli arrivino ordini dai client, il client di upload cercherà di inviare dei pacchetti al server, il client di download cercherà di scaricare i pacchetti che gli servono e quando avrà finito aspetterà che il server lo avverta che sono presenti nuove versioni dei pacchetti installati per provare a scaricarli nuovamente.

Per vedere nel dettaglio cosa sta succedendo dovrete aprire i file di log che vengono scritti durante l'esecuzione nella cartella `log` di ognuno dei programmi.

Il client di upload a un certo punto finirà di inviare i pacchetti al server e terminerà da solo. Server e client di download invece non terminano da soli e continuano a funzionare finchè l'utente non glielo dice esplicitamente premendo i tasti `CTRL` e `C` contemporaneamente in ognuna delle loro rispettive finestre. In ogni caso non termineranno subito ma passeranno un po' di secondi perchè devono chiudere i canali di comunicazione e pulire le strutture che hanno utilizzato.

## Licenza MIT

Copyright (c) 2013 Cristian Bellomo

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.