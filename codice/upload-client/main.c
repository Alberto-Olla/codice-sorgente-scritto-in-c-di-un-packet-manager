#include "main.h"

int main()
{	
	printf("Generatore di upload-client\n");
	
	while(connect_semaphores() == -1 && semval(SERVAVAILSEM) != 1)
	{
		printf("Server non disponibile. Nuovo tentativo di connessione fra 5 secondi...\n");
		sleep(5);
	}
	
	/* Ignora i segnali SIGINT e SIGTERM. Termina solo quando tutti i figli sono terminati (i figli terminano solo quando hanno eseguito tutte le istruzioni o il server non è più disponibile) */
	signal(SIGINT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	
	printf("Connesso al server. Generazione dei client d'upload in corso...\n");
	int i;
	for(i = 0; i < MAXUPCLIENT; i++) createclient(i);
	printf("Client generati\nTerminati 0 di %d client", MAXUPCLIENT);
	fflush(stdout);
	
	for(i = 0; i < MAXUPCLIENT; i++)
	{
		wait(NULL);
		printf("\r\e[0KTerminati %d di %d client", i+1, MAXUPCLIENT);
		fflush(stdout);
	}
	printf("\n");
	exit(0);
}