#include "../commons/client.h"

/*
	Crea un upload-client e gli assegna il file di log con nome i.log
*/
void createclient(int i)
{
	pid_t pid = fork();
	if(pid != 0) return; // Solo i figli devono continuare
	
	/* File di log */
	sprintf(logfilename, "logs/%d.log", i+1);
	logfile = fopen(logfilename, "a");
	
	/* Istruzioni da eseguire */
	sprintf(instructionsfilename, "instructions/%d", i+1);
	instructionsfile = fopen(instructionsfilename, "r");
	numinstructions = 0;
	
	while(fscanf(instructionsfile, "%s", &tmp) != EOF) // Lettura da file e memorizzazione delle istruzioni
	{
		if(strcmp(tmp, "ACCREDIT") == 0)
		{
			instructions[numinstructions].op = OPACCREDIT;
			fscanf(instructionsfile, "%d", &instructions[numinstructions].arg1); // Chiave
			numinstructions++;
		} else if(strcmp(tmp, "ADD") == 0)
		{
			instructions[numinstructions].op = OPADD;
			fscanf(instructionsfile, "%d", &instructions[numinstructions].arg1); // Nome pacchetto
			numinstructions++;
		} else if(strcmp(tmp, "UPGRADE") == 0)
		{
			instructions[numinstructions].op = OPUPDATE;
			fscanf(instructionsfile, "%d", &instructions[numinstructions].arg1); // Nome pacchetto
			fscanf(instructionsfile, "%d", &instructions[numinstructions].arg2); // Versione
			numinstructions++;
		}
	}
	fclose(instructionsfile);
	
	fprintf(logfile, "Client n.%d avviato (pid: %d)\n", i+1, getpid());
	
	/* Si prepara a comunicare col server */
	connect_messages();
	pid = getpid();
	msgtosend.type = TYPESERVER; // Inizializza il destinatario dei propri messaggi (il server)
	msgtosend.pid = pid; // Inizializza il mittente dei messaggi col proprio pid
	
	fprintf(logfile, "Numero di istruzioni da eseguire: %d\n", numinstructions);
	
	/* Esecuzione delle istruzioni */
	int currentinstruction = 0;
	while(semval(SERVAVAILSEM) == 1) // Continua fintantochè il server è disponibile
	{
		if(currentinstruction == numinstructions) // Se sono state eseguite tutte le istruzioni allora termina
		{
			fprintf(logfile, "Eseguite tutte le istruzioni. Terminazione in corso...\n\n");
			fflush(logfile);
			fclose(logfile);
			exit(0);
		} else {
			switch(instructions[currentinstruction].op)
			{
				case OPACCREDIT:
					fprintf(logfile, "Richiesta di accreditamento con chiave %d\n", instructions[currentinstruction].arg1);
					msgtosend.op = OPACCREDIT;
					msgtosend.name = instructions[currentinstruction].arg1;
					send_message(&msgtosend);
					receive_message(pid, 1, &msgreceived);
					if(msgreceived.op == OPSUCCESS) fprintf(logfile, "Accreditamento avvenuto con successo\n");
					else if(msgreceived.op == OPFAILURE) fprintf(logfile, "Impossibile accreditarsi\n");
					sleep(3);
				break;
				case OPADD:
					fprintf(logfile, "Richiesta di aggiunta per il pacchetto %d\n", instructions[currentinstruction].arg1);
					if(instructions[currentinstruction].arg1 > REPOLEN-1)
					{
						fprintf(logfile, "Errore: i nomi dei pacchetti vanno da 0 a %d (nome usato %d)\n", REPOLEN-1, instructions[currentinstruction].arg1);
						break;
					}
					msgtosend.op = OPADD;
					msgtosend.name = instructions[currentinstruction].arg1;
					send_message(&msgtosend);
					receive_message(pid, 1, &msgreceived);
					if(msgreceived.op == OPSUCCESS) fprintf(logfile, "Aggiunta pacchetto %d avvenuta con successo\n", instructions[currentinstruction].arg1);
					else if(msgreceived.op == OPFAILURE) fprintf(logfile, "Impossibile aggiungere il pacchetto %d\n", instructions[currentinstruction].arg1);
					sleep(3);
				break;
				case OPUPDATE:
					fprintf(logfile, "Richiesta di aggiornamento per il pacchetto %d alla versione %d\n", instructions[currentinstruction].arg1, instructions[currentinstruction].arg2);
					if(instructions[currentinstruction].arg1 > REPOLEN-1)
					{
						fprintf(logfile, "Errore: i nomi dei pacchetti vanno da 0 a %d (nome usato %d)\n", REPOLEN-1, instructions[currentinstruction].arg1);
						break;
					}
					msgtosend.op = OPUPDATE;
					msgtosend.name = instructions[currentinstruction].arg1;
					msgtosend.version = instructions[currentinstruction].arg2;
					send_message(&msgtosend);
					receive_message(pid, 1, &msgreceived);
					if(msgreceived.op == OPSUCCESS) fprintf(logfile, "Aggiornamento pacchetto %d avvenuto con successo\n", instructions[currentinstruction].arg1);
					else if(msgreceived.op == OPFAILURE) fprintf(logfile, "Impossibile aggiornare il pacchetto %d alla versione %d\n", instructions[currentinstruction].arg1, instructions[currentinstruction].arg2);
					sleep(3);
				break;
			}
			currentinstruction++;
		}
	}
	
	fprintf(logfile, "Server non più disponibile\nTerminazione in corso...\n\n");
	fflush(logfile);
	fclose(logfile);
	exit(0);
}