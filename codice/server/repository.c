#include "repository.h"

/*
	Alloca il repository.
	Ritorna -1 in caso di errore, 1 altrimenti
*/
int init_repository()
{
	/* Repository */
	if((reposhmid = shmget(IPC_PRIVATE, sizeof(int) * REPOLEN, IPC_CREAT | 0666)) == -1) return -1;
	repository = (int *)shmat(reposhmid, NULL, 0);
	int i;
	for(i = 0; i < REPOLEN; i++) repository[i] = -1; // Inizializza il repository (non c'è nessun pacchetto installato all'inizio)
	
	/* Numero lettori del repository */
	if((readersshmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666)) == -1) return -1;
	readers = (int *)shmat(readersshmid, NULL, 0);
	*readers = 0;
	
	
	/* Autorizzazioni */
	if((authshmid = shmget(IPC_PRIVATE, sizeof(pid_t) * MAXUPCLIENT, IPC_CREAT | 0666)) == -1) return -1;
	authorizations = (pid_t *)shmat(authshmid, NULL, 0);
	for(i = 0; i < MAXUPCLIENT; i++) authorizations[i] = 0; // Inizializza le autorizzazioni (non c'è nessun client autorizzato all'inizio)
	
	/* Numero lettori delle autorizzazioni */
	if((authreadersshmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666)) == -1) return -1;
	authreaders = (int *)shmat(authreadersshmid, NULL, 0);
	*authreaders = 0;
	
	
	/* Registrazioni al servizio push */
	if((pushshmid = shmget(IPC_PRIVATE, sizeof(pid_t) * MAXDOWNCLIENT, IPC_CREAT | 0666)) == -1) return -1;
	pushregistrations = (pid_t *)shmat(pushshmid, NULL, 0);
	for(i = 0; i < MAXDOWNCLIENT; i++) pushregistrations[i] = 0; // Inizializza le autorizzazioni (non c'è nessun client autorizzato all'inizio)
	
	/* Numero lettori delle registrazioni al servizio push */
	if((pushreadersshmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666)) == -1) return -1;
	pushreaders = (int *)shmat(pushreadersshmid, NULL, 0);
	*pushreaders = 0;
	
	/* Semafori */
	setsem(REPOMUTEXSEM, 1);
	setsem(REPOWRITESEM, 1);
	setsem(AUTHMUTEXSEM, 1);
	setsem(AUTHWRITESEM, 1);
	setsem(PUSHMUTEXSEM, 1);
	setsem(PUSHWRITESEM, 1);
	
	return 1;
}

/*
	Rimuove il repository.
*/
void deinit_repository()
{
	/* Avverte i client registrati al servizio push che il server sta terminando */
	int i;
	for(i = 0; i < MAXDOWNCLIENT; i++)
	{
		if(pushregistrations[i] != 0)
		{
			kill(pushregistrations[i], SIGUSR2);
		}
	}
	
	shmctl(reposhmid, IPC_RMID, 0);
	shmctl(readersshmid, IPC_RMID, 0);
	shmctl(authshmid, IPC_RMID, 0);
	shmctl(authreadersshmid, IPC_RMID, 0);
	shmctl(pushshmid, IPC_RMID, 0);
	shmctl(pushreadersshmid, IPC_RMID, 0);
}

/*
	Accredita il client d'upload che possiede il pid fornito se la chiave passata è uguale a AUTHKEY (ritorna 1) e se c'è spazio sufficiente per memorizzare l'autorizzazione.
	Ritorna 0 in caso di fallimento.
*/
int accredit(pid_t pid, int key)
{	
	if(key != AUTHKEY) return 0;
	
	int spacefound = 0; // Controlla se è stato trovato lo spazio per inserire l'autorizzazione perchè se non c'è allora l'autorizzazione non può essere portata a termine con successo
	
	P(AUTHWRITESEM);
		int i;
		for(i = 0; i < MAXUPCLIENT; i++)
		{
			if(authorizations[i] == 0)
			{
				authorizations[i] = pid;
				spacefound = 1;
				break;
			}
		}
	V(AUTHWRITESEM);
	
	return spacefound;
}

/* Controlla se il client d'upload di cui viene fornito il pid è accreditato e in tal caso ritorna 1, altrimenti 0 */
int accredited(pid_t pid)
{
	int returnvalue = 0;
	
	P(AUTHMUTEXSEM);
		*authreaders = *authreaders + 1;
		if(*authreaders == 1) P(AUTHWRITESEM);
	V(AUTHMUTEXSEM);
	
	int i;
	for(i = 0; i < MAXUPCLIENT; i++)
	{
		if(authorizations[i] == pid)
		{
			returnvalue = 1;
			break;
		}
	}
	
	P(AUTHMUTEXSEM);
		*authreaders = *authreaders - 1;
		if(*authreaders == 0) V(AUTHWRITESEM);
	V(AUTHMUTEXSEM);
	
	return returnvalue;
}

/*
	Registra al servizio push il client di download che possiede il pid fornito se c'è spazio sufficiente per memorizzare l'autorizzazione.
	Ritorna 0 in caso di fallimento.
*/
int pushregister(pid_t pid)
{	
	int spacefound = 0; // Controlla se è stato trovato lo spazio per inserire la registrazione perchè se non c'è allora la registrazione non può essere portata a termine con successo
		
	P(PUSHWRITESEM);
		int i;
		for(i = 0; i < MAXDOWNCLIENT; i++)
		{
			if(pushregistrations[i] == 0)
			{
				pushregistrations[i] = pid;
				spacefound = 1;
				break;
			}
		}
	V(PUSHWRITESEM);
	
	return spacefound;
}

/*
	Invia ai client di download registrati al servizio push un segnale (SIGUSR1) che li avverte che il repository è cambiato.
*/
void pushsend()
{
	P(PUSHMUTEXSEM);
		*pushreaders = *pushreaders + 1;
		if(*pushreaders == 1) P(PUSHWRITESEM);
	V(PUSHMUTEXSEM);
	
		int i;
		for(i = 0; i < MAXDOWNCLIENT; i++)
		{
			if(pushregistrations[i] != 0)
			{
				kill(pushregistrations[i], SIGUSR1);
			}
		}
	
	P(PUSHMUTEXSEM);
		*pushreaders = *pushreaders - 1;
		if(*pushreaders == 0) V(PUSHWRITESEM);
	V(PUSHMUTEXSEM);
}

/* 
	Ritorna le informazioni riguardanti il pacchetto col nome specificato (in realtà ritorna solo la versione).
	Se il pacchetto specificato non è presente allora ritorna -1.
*/
int packetinfo(int packetname)
{
	int tmp;
	
	P(REPOMUTEXSEM);
		*readers = *readers + 1;
		if(*readers == 1) P(REPOWRITESEM);
	V(REPOMUTEXSEM);
	
		tmp = repository[packetname];

	P(REPOMUTEXSEM);
		*readers = *readers - 1;
		if(*readers == 0) V(REPOWRITESEM);
	V(REPOMUTEXSEM);

	return tmp;
}

/*
	Aggiunge un nuovo pacchetto al repository.
	Ritorna -1 nel caso il pacchetto specificato sia già presente.
*/
int packetadd(int packetname)
{	
	if(packetinfo(packetname) == -1)
	{
		P(REPOWRITESEM);
			repository[packetname] = 0; // Quando aggiunge un nuovo pacchetto la sua versione è automaticamente impostata a 0
		V(REPOWRITESEM);
		return 1;
	}
	
	return -1;
}

/*
	Aggiorna la versione di pacchetto presente nel repository.
	Ritorna -1 nel caso il pacchetto specificato non sia presente o nel caso il numero versione specificato sia minore o uguale a quello del pacchetto presente nel repository.
*/
int packetupdate(int packetname, int version)
{
	int repoversion = packetinfo(packetname);
	
	if(repoversion != -1 && version > repoversion)
	{
		P(REPOWRITESEM);
			repository[packetname] = version; // Quando aggiunge un nuovo pacchetto la sua versione è automaticamente impostata a 0
		V(REPOWRITESEM);
		return 1;
	}
	
	return -1;
}