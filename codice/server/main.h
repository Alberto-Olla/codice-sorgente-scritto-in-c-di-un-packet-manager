#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "../commons/semaphores.h"
#include "../commons/msgqueue.h"
#include "../commons/repository.h"

/* Tiene traccia di quali moduli sono stati inizializzati*/
#define MODSEM 0 // Semafori
#define MODLOG 1 // Logger
#define MODREP 2 // Repository
#define MODMSG 3 // Coda messaggi

#define MODCOUNT 4 // Numero di moduli presenti

int initialized[MODCOUNT]; // 1 se il modulo è inizializzato, altrimenti 0