#include "logger.h"

/*
	Inizializza il logger.
	Ritorna -1 in caso di errore, 1 altrimenti
*/
int init_logger()
{
	/* Logfile */
	if((logfile = fopen(LOGFILENAME, "a")) == NULL) return -1; // Cerca di aprire il file di log in modalità append
	
	/* Buffer */
	if((buffershmid = shmget(IPC_PRIVATE, sizeof(char) * STRINGLEN * BUFFERLEN, IPC_CREAT | 0666)) == -1) return -1;
	buffer = (char *)shmat(buffershmid, NULL, 0);
	
	/* Numero messaggi di log */
	if((logsshmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666)) == -1) return -1;
	logs = (int *)shmat(logsshmid, NULL, 0);
	*logs = 0;
	
	/* Semaforo */
	setsem(LOGSEM, 1);
	
	return 1;
}

/*
	Rimuove il logger.
*/
void deinit_logger()
{
	if(*logs > 0) // Riversa sul file di log gli eventuali messaggi di log ancora su buffer
	{
		int i;
		for(i = 0; i < *logs; i++) fprintf(logfile, "%s", buffer + i * STRINGLEN);
		fflush(logfile);
	}
	
	shmctl(buffershmid, IPC_RMID, 0);
	shmctl(logsshmid, IPC_RMID, 0);
	fclose(logfile);
}

/*
	Scrive il messaggio specificato nel file di log.
	L'uso è simile a quello di una printf.
*/
void writelog(const char *format, ...)
{
	va_list list;
	va_start(list, format);
	
	P(LOGSEM);
		if(*logs > BUFFERLEN - 1) // Se il buffer è pieno lo riversa sul file insieme all'ultimo messaggio di log
		{
			int i;
			for(i = 0; i < BUFFERLEN; i++) fprintf(logfile, "%s", buffer + i * STRINGLEN);
			vfprintf(logfile, format, list);
			fflush(logfile);
			*logs = 0;
		} else {
			vsprintf(buffer + (*logs) * STRINGLEN, format, list);
			*logs = *logs + 1;
		}
	V(LOGSEM);
	
	va_end(list);
}