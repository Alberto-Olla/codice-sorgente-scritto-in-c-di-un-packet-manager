#include <stdlib.h> // Per la definizione di NULL
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
 #include <signal.h>
#include "../commons/upload-client.h"
#include "../commons/download-client.h"
#include "../commons/repository.h"
#include "../commons/semaphores.h"

/* Chiave che permette ai client di upload di accreditarsi */
#define AUTHKEY 12345


/* Definizione del repository */
int *repository; // E' un array dove repository[i] contiene il numero di versione dell'applicazione con nome ì (quando il numero di versione è -1 allora vuol dire che quella applicazione non c'è)
int *readers; // Numero di lettori del repository

/* Id della memoria condivisa per il repository */
int reposhmid;

/* Id della memoria condivisa per tenere il conto del numero di lettori del repository */
int readersshmid;



/* Definizione delle autorizzazioni */
pid_t *authorizations; // E' un array dove ogni entry contiene il pid di un client di upload accreditato (quando l'entry contiene 0 allora vuol dire che non contiene nessun client autorizzato)
int *authreaders; // Numero di lettori delle autorizzazioni

/* Id della memoria condivisa per le autorizzazioni */
int authshmid;

/* Id della memoria condivisa per tenere il conto del numero di lettori delle autorizzazioni */
int authreadersshmid;



/* Definizione delle registrazioni push */
pid_t *pushregistrations; // E' un array dove ogni entry contiene il pid di un client di download registrato al servizio push (quando l'entry contiene 0 allora vuol dire che non contiene nessun client registrato)
int *pushreaders; // Numero di lettori delle registrazioni al servizio push

/* Id della memoria condivisa per le registrazioni al servizio push */
int pushshmid;

/* Id della memoria condivisa per tenere il conto del numero di lettori delle registrazioni al servizio push */
int pushreadersshmid;