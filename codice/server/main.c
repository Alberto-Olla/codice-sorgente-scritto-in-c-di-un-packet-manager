#include "main.h"

/*
	Segnala al server che non deve più accettare nuove richieste e ai client che non devono più inviarne di nuove.
	Usa un semaforo.
*/
void endingsequence()
{
	signal(SIGINT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	
	setsem(SERVAVAILSEM, UNAVAIBLE); // Segnala a se stesso e ai client che il server non è più disponibile per ricevere richieste perchè sta terminando
}

/* Deinizializzatore */
void deinit()
{
	/* Ignora i segnali SIGINT e SIGTERM */
	signal(SIGINT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	
	// ...
	if(initialized[MODMSG] == 1) deinit_messages();
	if(initialized[MODREP] == 1) deinit_repository();
	if(initialized[MODLOG] == 1) deinit_logger();
	if(initialized[MODSEM] == 1) deinit_semaphores();
	return;
}

/* 
	Inizializzatore 
	Ritorna -1 in caso di errore, 1 altrimenti
*/
int init()
{
	/* Ignora i segnali SIGINT (Ctrl-C) e SIGTERM (segnale di default quando si usa kill) e SIGCHLD (non ci interessa l'exit status dei figli) */
	signal(SIGINT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGCHLD, SIG_IGN);
	
	int i;
	for(i=0; i < MODCOUNT; i++) initialized[i] = 0; // Inizializza l'array che tiene traccia dei moduli avviati
	
	/* Semafori */
	if(init_semaphores() == -1) return -1;
	else initialized[MODSEM] = 1;
	
	/* Logger */
	if(init_logger() == -1) return -1;
	else initialized[MODLOG] = 1;
	
	/* Repository */
	if(init_repository() == -1) return -1;
	else initialized[MODREP] = 1;
	
	/* Coda messaggi */
	if(init_messages() == -1) return -1;
	else initialized[MODMSG] = 1;
	
	setsem(CHILDSEM, MAXCHILD); // Inizializza il semaforo per regolare lo spawning di figli da parte del server
	setsem(SERVAVAILSEM, AVAIBLE); // Segnala ai client che il server è disponibile per ricevere richieste
	
	/* D'ora in poi i segnali SIGINT e SIGTERM saranno gestiti da endingsequence */
	signal(SIGINT, endingsequence);
	signal(SIGTERM, endingsequence);
	
	return 1;
}

/*
	Soddisfa la richiesta specificata (affidandola a un figlio).
	Se il server rileva che c'è un errore nella ricezione del messaggio allora il server viene terminato.
*/
void satisfyrequest(struct message *request)
{	
	P(CHILDSEM); // Aspetta finchè non è possibile spawnare un figlio
	
	pid_t pid = fork();
	if(pid != 0) return; // Solo i figli devono continuare per soddisfare la richiesta
	
	/* Ignora i segnali SIGINT e SIGTERM */
	signal(SIGINT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	
	/* Identifica il tipo di richiesta e la soddisfa */
	switch(request->op)
	{
		case OPACCREDIT:
			writelog("Richiesta di accreditamento da parte di %d con chiave %d\n", request->pid, request->name);
			/* L'accreditamente viene effettuato solo se il client non è già stato accreditato e ha la chiave giusta e c'è abbastanza spazio per memorizzare l'accreditamento */
			if(accredited(request->pid))
			{
				request->op = OPSUCCESS; // E' già accreditato
				writelog("%d già accreditato\n", request->pid);
			} else if(accredit(request->pid, request->name))
			{
				request->op = OPSUCCESS; // In questo caso name contiene la chiave
				writelog("%d accreditato\n", request->pid);
			} else {
				request->op = OPFAILURE;
				writelog("Impossibile accreditare %d\n", request->pid);
			}
			request->type = request->pid;
			send_message(request); // Feedback
		break;
		case OPPUSH:
			writelog("Richiesta di registrazione a servizio push da parte di %d\n", request->pid);
			/* La registrazione al servizio push viene effettuata solo se c'è abbastanza spazio  */
			if(pushregister(request->pid))
			{
				request->op = OPSUCCESS; // C'è abbastanza spazio
				writelog("%d registrato al servizio push\n", request->pid);
			} else {
				request->op = OPFAILURE;
				writelog("Impossibile registrare %d al servizio push\n", request->pid);
			}
			request->type = request->pid;
			send_message(request); // Feedback
		break;
		case OPINFO:
			writelog("Richiesta di informazioni sul pacchetto %d da parte di %d\n", request->name, request->pid);
			/* Trasmette le informazioni su uno specifico pacchetto */
			request->type = request->pid;
			request->version = packetinfo(request->name);
			send_message(request);
			writelog("Inviate a %d informazioni sul pacchetto %d\n", request->pid, request->name);
		break;
		case OPINFOALL:
			writelog("Richiesta di informazioni sul contenuto del repository da parte di %d\n", request->pid);
			/* Trasmette le informazioni su uno specifico pacchetto */
			request->type = request->pid;
			int i;
			for(i = 0; i < REPOLEN; i++)
			{
				if(packetinfo(i) != -1) // Invia le informazioni solo per i pacchetti presenti nel repository
				{
					request->name = i;
					request->version = packetinfo(i);
					send_message(request);
				}
			}
			request->op = OPEND;
			send_message(request);
			writelog("Inviate a %d informazioni sul contenuto del repository\n", request->pid);
		break;
		case OPADD:
			writelog("Richiesta di aggiunta del pacchetto %d da parte di %d\n", request->name, request->pid);
			/* L'aggiunta di un pacchetto viene effettuata solo se il client di upload che fa la richiesta è accreditato */
			if(accredited(request->pid) && packetadd(request->name) != -1)
			{
				request->op = OPSUCCESS;
				pushsend();
				writelog("Aggiunto pacchetto %d proveniente da %d e inviata notifica del cambiamento repository ai client registrati al servizio push\n", request->name, request->pid);
			} else {
				request->op = OPFAILURE;
				writelog("Impossibile aggiungere pacchetto %d proveniente da %d\n", request->name, request->pid);
			}
			request->type = request->pid;
			send_message(request); // Feedback
		break;
		case OPUPDATE:
			writelog("Richiesta di aggiornamento del pacchetto %d alla versione %d da parte di %d\n", request->name, request->version, request->pid);
			/* L'aggiornamento di un pacchetto viene effettuata solo se il client di upload che fa la richiesta è accreditato e se la versione del pacchetto che fornisce è maggiore di quella del pacchetto su server */
			if(accredited(request->pid) && packetupdate(request->name, request->version) != -1)
			{
				request->op = OPSUCCESS;
				pushsend();
				writelog("Aggiornato pacchetto %d su richiesta di %d e inviata notifica del cambiamento repository ai client registrati al servizio push\n", request->name, request->pid);
			} else {
				request->op = OPFAILURE;
				writelog("Impossibile aggiornare pacchetto %d alla versione %d su richiesta di %d\n", request->name, request->version, request->pid);
			}
			request->type = request->pid;
			send_message(request); // Feedback
		break;
	}
	
	V(CHILDSEM); // Segnala che un figlio è terminato
	exit(0);
}

/* 
	Main
*/
int main(void)
{
	if(init() == -1)
	{
		fprintf(stderr, "Errore: impossibile inizializzare il server!\n");
		deinit();
		exit(1);
	}
	
	printf("Server avviato\n");
	writelog("Server pronto\nIn attesa di richieste...\n");
	
	struct message request;
	
	while(semval(SERVAVAILSEM) == AVAIBLE) // Ciclo principale
	{
		receive_message(TYPESERVER, 1, &request);
		satisfyrequest(&request);
	}
	
	printf("Terminazione in corso...");
	writelog("Ordine di terminazione ricevuto\nVerranno ora soddisfatte le richieste rimaste in coda prima di terminare\n");
	
	/* Finisce di soddisfare le richieste rimanenti prima di terminare */
	receive_message(TYPESERVER, 0, &request);
	while(request.op != MSGERROR) // Finchè ci son richieste rimanenti sulla coda
	{
		satisfyrequest(&request);
		receive_message(TYPESERVER, 0, &request);
	}
	sleep(5); // Per dare tempo ai figli di soddisfare le richieste
	writelog("Tutte le richieste rimanenti sono state soddisfatte\nTerminazione in corso...\n");	
	
	writelog("Stato del repository al momento della chiusura:\n");
	int i;
	for(i = 0; i < REPOLEN; i++) // Prima di terminare stampa lo stato del repository
	{
		if(packetinfo(i) != -1) writelog("  - Pacchetto %d versione %d\n", i, packetinfo(i));
	}
	writelog("\n");
	
	deinit();
	exit(0);
}