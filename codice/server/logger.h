#include <stdio.h>
#include <stdarg.h>
#include <sys/shm.h>
#include "../commons/semaphores.h"

/* Logfile */
#define LOGFILENAME "server.log" // Nome del file di log
FILE *logfile; // Riferimento al file di log

/* Definizione del buffer */
#define BUFFERLEN 5 // Quanti elementi puo' contenere il buffer
#define STRINGLEN 200 // Quanti caratteri ogni elemento del buffer può contenere
char *buffer; // E' un array dove ogni entry contiene un messaggio di log in attesa di essere scritto su file
int *logs; // Numero di messaggi log del buffer

/* Id della memoria condivisa per il buffer*/
int buffershmid;

/* Id della memoria condivisa per tenere il conto del numero di messaggi di log nel buffer */
int logsshmid;