#include "client.h"

void push()
{
	signal(SIGUSR1, push);
}
/*
	Termina il client quando il server non è piu disponibile o quando viene ricevuto il segnale SIGUSR2 (che avvisa i client iscritti al servizio push che il server sta terminando)
*/
void endingsequence()
{
	fprintf(logfile, "Server non più disponibile\nTerminazione in corso...\nStato del repository al momento della chiusura:\n");
	int i;
	for(i = 0; i < REPOLEN; i++) // Prima di terminare stampa lo stato del repository
	{
		if(repository[i] != -1) fprintf(logfile, "  - Pacchetto %d versione %d\n", i, repository[i]);
	}
	fprintf(logfile, "\n");
	
	fflush(logfile);
	fclose(logfile);
	exit(0);
}

/*
	Crea un download-client e gli assegna il file di log con nome i.log
*/
void createclient(int num)
{
	pid_t pid = fork();
	if(pid != 0) return; // Solo i figli devono continuare
	
	/* File di log */
	sprintf(logfilename, "logs/%d.log", num+1);
	logfile = fopen(logfilename, "a");
	
	/* Istruzioni da eseguire */
	sprintf(instructionsfilename, "instructions/%d", num+1);
	instructionsfile = fopen(instructionsfilename, "r");
	numinstructions = 0;
	
	while(fscanf(instructionsfile, "%s", &tmp) != EOF) // Lettura da file e memorizzazione delle istruzioni
	{
		if(strcmp(tmp, "PUSH") == 0)
		{
			instructions[numinstructions].op = OPPUSH;
			numinstructions++;
		} else if(strcmp(tmp, "INSTALL") == 0)
		{
			instructions[numinstructions].op = OPINFO;
			fscanf(instructionsfile, "%d", &instructions[numinstructions].arg1); // Nome pacchetto
			numinstructions++;
		} else if(strcmp(tmp, "UPDATE") == 0)
		{
			instructions[numinstructions].op = OPUPDATE;
			fscanf(instructionsfile, "%d", &instructions[numinstructions].arg1); // Nome pacchetto
			numinstructions++;
		}
	}
	fclose(instructionsfile);
	
	fprintf(logfile, "Client n.%d avviato (pid: %d)\n", num+1, getpid());
	
	/* Inizializza il repository */
	int i;
	for(i = 0; i < REPOLEN; i++) repository[i] = -1;
	
	/* Il client non è ancora registrato al servizio push */
	int pushregistered = 0;
	
	/* Si prepara a comunicare col server */
	connect_messages();
	pid = getpid();
	msgtosend.type = TYPESERVER; // Inizializza il destinatario dei propri messaggi (il server)
	msgtosend.pid = pid; // Inizializza il mittente dei messaggi col proprio pid
	
	/* Prima di iniziare sincronizza il proprio repository con quello del server */
	fprintf(logfile, "Sincronizzazione iniziale del repository in corso\n");
	msgtosend.op = OPINFOALL;
	send_message(&msgtosend);
	receive_message(pid, 1, &msgreceived);
	while(msgreceived.op != OPEND)
	{
		repository[msgreceived.name] = msgreceived.version;
		fprintf(logfile, "  + Installato il programma %d versione %d\n", msgreceived.name, msgreceived.version);
		receive_message(pid, 1, &msgreceived);
	}
	fprintf(logfile, "Repository sincronizzato\n");
	
	fprintf(logfile, "Numero di istruzioni da eseguire: %d\n", numinstructions);
	
	/* Esecuzione delle istruzioni */
	int currentinstruction = 0;
	while(semval(SERVAVAILSEM) == 1) // Continua fintantochè il server è disponibile
	{
		fflush(logfile);
		if(currentinstruction == numinstructions) // Se sono state eseguite tutte le istruzioni allora ogni tot secondi richiede al server se qualcosa è cambiato nel repository
		{
			fprintf(logfile, "Sincronizzazione periodica del repository in corso\n");
			msgtosend.op = OPINFOALL;
			send_message(&msgtosend);
			receive_message(pid, 1, &msgreceived);
			while(msgreceived.op != OPEND)
			{
				if(repository[msgreceived.name] == -1) // Se l'applicazione non è presente allora la installa
				{
					repository[msgreceived.name] = msgreceived.version;
					fprintf(logfile, "  + Installato il programma %d versione %d\n", msgreceived.name, msgreceived.version);
				} else if(repository[msgreceived.name] < msgreceived.version) // Se l'applicazione è presente ed è più vecchia di quella del server allora l'aggiorna
				{
					repository[msgreceived.name] = msgreceived.version;
					fprintf(logfile, "  o Aggiornato il programma %d alla versione %d\n", msgreceived.name, msgreceived.version);
				}
				receive_message(pid, 1, &msgreceived);
			}
			fprintf(logfile, "Repository sincronizzato\n");
			sleep(15);
		} else {
			switch(instructions[currentinstruction].op)
			{
				case OPPUSH:
					if(!pushregistered) // Se non si è registrati al servizio push allora cerca di registrarsi
					{
						fprintf(logfile, "Richiesta di iscrizione al servizio push\n");
						msgtosend.op = OPPUSH;
						send_message(&msgtosend);
						receive_message(pid, 1, &msgreceived);
						if(msgreceived.op == OPSUCCESS)
						{
							fprintf(logfile, "Iscrizione al servizio push avvenuta con successo\n");
							/* Inizializza la variabile che serve a implementare la sospensione dell'esecuzione in attesa del segnale di push */
							pushregistered = 1;
							signal(SIGUSR2, endingsequence); // Se gli viene notificato che il server non è più disponibile allora termina
							signal(SIGUSR1, push);
						}
						else if(msgreceived.op == OPFAILURE)
						{
							fprintf(logfile, "Impossibile iscriversi al servizio push\n");
							currentinstruction++; // Se l'iscrizione al servizio push non ha avuto successo allora continua con l'esecuzione delle istruzioni successive
							sleep(3);
						}
					} else {
						pause();
						fprintf(logfile, "Ricevuta notifica push dal server\nSincronizzazione repository in corso\n");
						msgtosend.op = OPINFOALL;
						send_message(&msgtosend);
						receive_message(pid, 1, &msgreceived);
						while(msgreceived.op != OPEND)
						{
							if(repository[msgreceived.name] == -1) // Se l'applicazione non è presente allora la installa
							{
								repository[msgreceived.name] = msgreceived.version;
								fprintf(logfile, "  + Installato il programma %d versione %d\n", msgreceived.name, msgreceived.version);
							} else if(repository[msgreceived.name] < msgreceived.version) // Se l'applicazione è presente ed è più vecchia di quella del server allora l'aggiorna
							{
								repository[msgreceived.name] = msgreceived.version;
								fprintf(logfile, "  o Aggiornato il programma %d alla versione %d\n", msgreceived.name, msgreceived.version);
							}
							receive_message(pid, 1, &msgreceived);
						}
						fprintf(logfile, "Repository sincronizzato\n");
					}
				break;
				case OPINFO:
					fprintf(logfile, "Richiesta di installazione per il programma %d\n", instructions[currentinstruction].arg1);
					/* Se il pacchetto è già installato non cerca di reinstallarlo */
					if(instructions[currentinstruction].arg1 > REPOLEN-1) fprintf(logfile, "Errore: i nomi dei pacchetti vanno da 0 a %d (nome usato %d)\n", REPOLEN-1, instructions[currentinstruction].arg1);
					else if(repository[instructions[currentinstruction].arg1] != -1) fprintf(logfile, "Il pacchetto %d è già installato\n", instructions[currentinstruction].arg1);
					else { // Il pacchetto non è ancora stato installato
						msgtosend.op = OPINFO;
						msgtosend.name = instructions[currentinstruction].arg1;
						send_message(&msgtosend);
						receive_message(pid, 1, &msgreceived);
						if(msgreceived.version != -1 )
						{
							repository[msgreceived.name] = msgreceived.version;
							fprintf(logfile, "Installazione pacchetto %d versione %d avvenuta con successo\n", msgreceived.name, msgreceived.version);
						} else fprintf(logfile, "Il pacchetto %d non è stato trovato sul server\n", msgreceived.name);
					}
					currentinstruction++;
					sleep(3);
				break;
				case OPUPDATE:
					fprintf(logfile, "Richiesta di aggiornamento per il programma %d\n", instructions[currentinstruction].arg1);
					/* Se il pacchetto non è installato allora non può aggiornarlo */
					if(instructions[currentinstruction].arg1 > REPOLEN-1) fprintf(logfile, "Errore: i nomi dei pacchetti vanno da 0 a %d (nome usato %d)\n", REPOLEN-1, instructions[currentinstruction].arg1);
					else if(repository[instructions[currentinstruction].arg1] == -1) fprintf(logfile, "Il pacchetto %d non è installato\n", instructions[currentinstruction].arg1);
					else { // Il pacchetto è installato
						msgtosend.op = OPINFO;
						msgtosend.name = instructions[currentinstruction].arg1;
						send_message(&msgtosend);
						receive_message(pid, 1, &msgreceived);
						if(msgreceived.version != -1 )
						{
							if(msgreceived.version > repository[instructions[currentinstruction].arg1])
							{
								fprintf(logfile, "Aggiornamento pacchetto %d dalla versione %d alla versione %d avvenuta con successo\n", msgreceived.name, repository[instructions[currentinstruction].arg1], msgreceived.version);
								repository[msgreceived.name] = msgreceived.version;
							} else fprintf(logfile, "Il pacchetto %d è già all'ultima versione\n", msgreceived.name);
							
						} else fprintf(logfile, "Il pacchetto %d non è stato trovato sul server\n", msgreceived.name);
					}
					currentinstruction++;
					sleep(3);
				break;
				default:
					currentinstruction++;
				break;
			}
		}
	}
	
	endingsequence();
}