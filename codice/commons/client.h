#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../commons/msgqueue.h"
#include "../commons/semaphores.h"
#include "../commons/repository.h"

/* Mantiene il riferimento al file di log */
FILE *logfile;

/* Nome del file di log */
char logfilename[11];

/* Mantiene il riferimento al file di istruzioni */
FILE *instructionsfile;

/* Nome del file di istruzioni */
char instructionsfilename[15];

/* Definizione di un istruzione */
struct instruction {
	int op;
	int arg1;
	int arg2;
};

/* Lista delle istruzioni (può contenere al massimo 20 istruzioni)*/
struct instruction instructions[20];
int numinstructions; // Numero delle istruzioni memorizzate
char tmp[300]; // Variabile in cui salvare temporaneamente le stringhe lette dal file di istruzioni

/* Comunicazione col server */
struct message msgtosend; // Usato per spedire i messaggi
struct message msgreceived; // Usato per ricevere i messaggi