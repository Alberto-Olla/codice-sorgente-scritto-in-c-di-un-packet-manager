#include "msgqueue.h"

/*
	Alloca la coda messaggi.
	Ritorna -1 in caso di errore, 1 altrimenti
*/
int init_messages()
{
	if((msqid = msgget(MSGKEY, 0666 | IPC_CREAT)) == -1) return -1;
	return 1;
}

/*
	Accede a una coda messaggi già esistente.
	Ritorna -1 in caso di errore, 1 altrimenti
*/
int connect_messages()
{
	if((msqid = msgget(MSGKEY, 0)) == -1) return -1;
	return 1;
}

/*
	Rimuove la coda messaggi.
*/
void deinit_messages()
{
	msgctl(msqid, IPC_RMID, NULL);
}

/*
	Inserisce un messaggio sulla coda.
	Ritorna -1 in caso di errore, 1 altrimenti
*/
int send_message(struct message *msgtosend)
{
	if(msgsnd(msqid, msgtosend, sizeof(struct message) - sizeof(long), 0) == -1) return -1;
	return 1;
}

/*
	Riceve un messaggio del tipo specificato dalla coda.
	In caso di assenza di messaggi ne aspetta l'arrivo se specificato (mustwait = 1 per aspettare).
	Scrive su buf un messaggio con tipo di operazione uguale a MSGERROR in caso di errore, altrimenti il messaggio ricevuto
*/
void receive_message(int mtype, int mustwait, struct message *buf)
{
	if(msgrcv(msqid, buf, sizeof(struct message) - sizeof(long), mtype, (mustwait == 1) ? 0 : IPC_NOWAIT) == -1)
	{
		buf->op = MSGERROR;
	}
}