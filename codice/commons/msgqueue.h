#include <stdlib.h> // Per la definizione di NULL
#include <sys/types.h>
#include <sys/msg.h>

#define MSGKEY 36542 // Chiave comune per accedere alla coda messaggi

/* Definizione di un messaggio */
struct message {
	long type; // 0 quando i client effettuano una richiesta, pid di un client quando il server vuole comunicare con esso
	pid_t pid; // Contiene il pid del client che effettua una richiesta, è 0 quando è il server a comunicare
	int op; // Tipo di operazione da eseguire
	/* Informazioni pacchetto */
	int name;
	int version;
};

/* Tipi di messaggio */
#define TYPESERVER 1 // Messaggio messaggio per il server
/* Non sono specificati altri tipi di messaggio perchè quando il server ne deve mandare uno usa il pid del processo che gli ha inviato il messaggio come tipo */

/* Tipi di operazione*/
#define MSGERROR -1 // Si è verificato un errore nella ricezione di un messaggio
#define OPFAILURE 0 // Segnala che l'operazione richiesta ha avuto successo
#define OPSUCCESS 1 // Segnala che l'operazione richiesta è fallita
#define OPEND 2 // Segnala che la serie di operazioni richiesta è stata portata a termine (es. quando il server ha finito di inviare tutti i pacchetti che costituiscono il repository a un client di download)
#define OPACCREDIT 3 // Richiesta di accreditamento da parte di un client di update
#define OPPUSH 4 // Richiesta di registrazione a servizio push da parte di un client di download
#define OPINFO 5 // Richiesta informazioni su un pacchetto
#define OPINFOALL 6 // Richiesta informazioni su tutti i pacchetti presenti nel repository
#define OPADD 7 // Richiesta di aggiunta di un pacchetto da parte di un client di update
#define OPUPDATE 8 // Richiesta di aggiornamento di un pacchetto da parte di un client di update

/* Id della coda messaggi */
int msqid;