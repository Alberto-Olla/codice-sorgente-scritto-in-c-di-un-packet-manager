#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#define SEMKEY 28635 // Chiave comune per accedere ai semafori
#define NUMSEM 9 // Numero semafori allocati

/* Costanti che definiscono il ruolo di ogni semaforo*/
#define SERVAVAILSEM 0 // Mutex che indica se il server è disponibile per ricevere nuove richieste (1) o meno (0)
#define LOGSEM 1 // Mutex per la scrittura sul file di log
#define REPOMUTEXSEM 2 // Mutex per aggiornare il numero di lettori del repository
#define REPOWRITESEM 3 // Mutex per modificare il repository
#define AUTHMUTEXSEM 4 // Mutex per aggiornare il numero di lettori delle autorizzazioni
#define AUTHWRITESEM 5 // Mutex per modificare le autorizzazioni
#define PUSHMUTEXSEM 6 // Mutex per aggiornare il numero di lettori delle registrazioni per il servizio push
#define PUSHWRITESEM 7 // Mutex per modificare le registrazioni per il servizio push
#define CHILDSEM 8 // Semaforo per regolare lo spawning di figli da parte del server

/* Costanti di comodità per i valori di alcuni semafori */
#define AVAIBLE 1 // Server disponibile
#define UNAVAIBLE 0 // Server non disponibile
#define MAXCHILD 5 // Numero massimo di figli che il server può avere a ogni istante

/* Id del semaforo */
int semid;