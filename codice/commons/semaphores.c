#include "semaphores.h"

/*
	Alloca i semafori.
	Ritorna -1 in caso di errore, 1 altrimenti
*/
int init_semaphores()
{
	if((semid = semget(SEMKEY, NUMSEM, 0666 | IPC_CREAT)) == -1) return -1;
	return 1;
}

/*
	Si connette a una struttura di semafori già esistente.
	Ritorna -1 in caso di errore, 1 altrimenti
*/
int connect_semaphores()
{
	if((semid = semget(SEMKEY, NUMSEM, 0)) == -1) return -1;
	return 1;
}

/*
	Rimuove i semafori.
*/
void deinit_semaphores()
{
	semctl(semid, 0, IPC_RMID, 0);
}

/*
	Restituisce il valore del semaforo indicato, - 1 in caso di errore.
*/
int semval(int semnum)
{
	return semctl(semid, semnum, GETVAL, 0);
}

/*
	Setta il valore di un semaforo a "val".
*/
void setsem(int semnum, int val)
{
	semctl(semid, semnum, SETVAL, val);
}

/*
	Funzione per la wait.
*/
int P(int semnum)
{
	struct sembuf cmd;
	cmd.sem_num = semnum;
	cmd.sem_op = -1;
	cmd.sem_flg = 0;
	semop(semid, &cmd, 1);
}

/*
	Funzione per la signal.
*/
int V(int semnum)
{
	struct sembuf cmd;
	cmd.sem_num = semnum;
	cmd.sem_op = 1;
	cmd.sem_flg = 0;
	semop(semid, &cmd, 1);
}
